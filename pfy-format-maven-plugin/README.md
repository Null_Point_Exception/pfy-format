# pfy-format-maven-plugin

私人定制配置文件对java代码进行格式化的项目

本工程为 spring-javaformat-maven-plugin 项目的简化 copy

## 项目说明

### pfy-format-maven-plugin

源码为spring-javaformat

spring-javaformat github：<https://github.com/spring-io/spring-javaformat>

依赖分支为v0.0.25

项目将原项目的 spring-javaformat-maven/spring-javaformat-maven-plugin 和 spring-javaformat/spring-javaformat-formatter 代码合并后的项目，达到自定义代码格式化配置文件 formatter.prefs 的目的

## 代码格式化

添加依赖

```xml
<build>
    <plugins>
        <plugin>
            <groupId>com.panfeiyun.project</groupId>
            <artifactId>pfy-format-maven-plugin</artifactId>
            <version>1.0-SNAPSHOT</version>
            <executions>
                <execution>
                    <phase>validate</phase>
                    <inherited>true</inherited>
                    <goals>
                        <goal>apply</goal>
                    </goals>
                </execution>
            </executions>
            <dependencies>
                <dependency>
                    <groupId>io.spring.javaformat</groupId>
                    <artifactId>spring-javaformat-formatter-eclipse</artifactId>
                    <version>0.0.25</version>
                </dependency>
                <dependency>
                    <groupId>io.spring.javaformat</groupId>
                    <artifactId>spring-javaformat-formatter-eclipse-runtime</artifactId>
                    <version>0.0.25</version>
                </dependency>
            </dependencies>
        </plugin>
    </plugins>
</build>
```

执行格式化

```bash
mvn com.panfeiyun.project:pfy-format-maven-plugin:1.0-SNAPSHOT:apply
```

## todo

从外部读取配置文件

添加githook











